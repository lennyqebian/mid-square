package midsquare;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author leonardoquevedo
 */
public class RandomGenerator {

    protected double x_0;

    protected String noDecimals = "%.0f";

    public RandomGenerator(double x_0) throws Exception {
        this.x_0 = x_0;
        String x_0Str = String.format(this.noDecimals, x_0);

        if (!(x_0Str.length() % 2 == 0)) {
            throw new Exception("¡La semilla x₀ no tiene 2n (número par de) cifras!");
        }
    }

    public double[] random(int n) throws Exception {
        if (!(n > 0)) {
            throw new Exception("¡La cantidad de números aleatorios a generar debe ser mayor que cero (0)!");
        }
        ArrayList<Double> X = new ArrayList();
        int ciphersQty, midSquare, midXi;
        double x_iSquare;
        String x_iSquareStr, x_iStr;
        double[] U = new double[n];
        X.add(this.x_0);

        for (int i = 0; i < n; i++) {
            ciphersQty = String.format(this.noDecimals, X.get(i)).length();
            midXi = ciphersQty / 2;
            x_iSquare = Math.pow(X.get(i), 2);
            x_iSquareStr = String.format(this.noDecimals, x_iSquare);

            if (x_iSquareStr.length() < 2 * ciphersQty) {
                x_iSquareStr = String.format("%0" + (2 * ciphersQty) + ".0f", x_iSquare);
            }
            midSquare = x_iSquareStr.length() / 2;
            x_iStr = x_iSquareStr.substring(midSquare - midXi, midSquare)
                    + x_iSquareStr.substring(midSquare, midSquare + midXi);
            U[i] = Double.parseDouble("0." + x_iStr);
            X.add(Double.parseDouble(x_iStr));
        }
        return U;
    }

    public String randomStr(int n, int decimalPlaces) throws Exception {
        double[] U = this.random(n);
        String format;

        if (!(decimalPlaces > 1)) {
            throw new Exception("¡El número de dígitos decimales debería ser por lo menos 2!");
        }
        format = "%." + decimalPlaces + "f";

        if (U.length > 1) {
            List<String> output = Arrays.stream(U).boxed().map((aDouble) -> {
                return String.format(format, aDouble);
            })
                    .collect(Collectors.toList());
            return "Números aleatorios generados: \n\n"
                    + String.join("\n", output);
        }
        return "Número aleatorio generado: " + String.format(format, U[0]);
    }

    public String randomStr(int n) throws Exception {
        return randomStr(n, 4);
    }

    public double random() throws Exception {
        return this.random(1)[0];
    }
}
